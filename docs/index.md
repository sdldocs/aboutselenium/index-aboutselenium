편역된 Selenium 문서들에 대한 index page이다.

## From [Selenium](https://www.selenium.dev/)
- [Selenium IDE](https://sdldocs.gitlab.io/aboutselenium/selenium-ide/)
- [The Selenium Browser Automation Project](https://sdldocs.gitlab.io/aboutselenium/selenium-browser-automation-project/)

## From [LAMBDATEST](https://www.lambdatest.com/)
- [Selenium Tutorial](https://sdldocs.gitlab.io/aboutselenium/seleniumtutorial/)

> **Note**: Selenium을 처음 공부하는 사용자는 아래 순서로 읽어 보는 것을 추천한다.
> 
> 1. [Selenium Tutorial](https://sdldocs.gitlab.io/aboutselenium/seleniumtutorial/)에서 "시작"과 "Tutorials" 섹션를 제외한 [Selenium 이란](https://sdldocs.gitlab.io/aboutselenium/seleniumtutorial/what-is-selenium/) 부터 [Selenium WebDriver를 이용한 테스트 자동화](https://sdldocs.gitlab.io/aboutselenium/seleniumtutorial/test-automation-with-selenium-webdriver/)까지 읽어  Selenium 전체에 대하여 이해에서 출발한다.
> 2. [Selenium IDE](https://sdldocs.gitlab.io/aboutselenium/selenium-ide/)의 "소개" 섹션
> 3. [The Selenium Browser Automation Project](https://sdldocs.gitlab.io/aboutselenium/selenium-browser-automation-project/)의 [The Selenium 브라우저 자동화 프로젝트](https://sdldocs.gitlab.io/aboutselenium/selenium-browser-automation-project/), [Selenium 개요](https://sdldocs.gitlab.io/aboutselenium/selenium-browser-automation-project/overview/), [WebDriver](https://sdldocs.gitlab.io/aboutselenium/selenium-browser-automation-project/webdriver/)과 [테스트 실습](https://sdldocs.gitlab.io/aboutselenium/selenium-browser-automation-project/test-practices/) 섹션 
>
> 위의 2와 3은 순서를 바꾸어도 무방하나 순서대로 읽는 것이 빠른 사용에 도움이 될 것이다.

